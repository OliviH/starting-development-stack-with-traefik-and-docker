# Docker Traefik  -  [(Version française)](../doc/fr/00-traefik/README.md)

## Installation

```bash
make install
```

This will install [mkcert](https://github.com/FiloSottile/mkcert) on your computer
and generate a self-signed certificate for `*.docker.localhost` domains. Also mkcert
will install a local CA authority that will recognize the previously generated
self-signed certificate, and allow it on your system and your browsers. So you
will have to manually accept it.

It will then create a docker network named `traefik`.

## Usage

```shell script
make docker-upd
```

This way Traefik will restart automatically when you start your computer and no more need of having a terminal opened with Traefik running.

```shell script
make docker-stop
```

This command stop traefik

To see more command use autocompletion after make command.