# Docker Traefik  -  [(English version)](../../../00-traefik/README.md)

## Installation

```bash
make install
```

Cette commande installe [mkcert](https://github.com/FiloSottile/mkcert) sur votre ordinateur et génère un certificat autosigné sur le domaine `*.docker.localhost`.

Aussi `mkcert` installera une autorité CA locale qui reconnaîtra le certificat auto-signé et autorisez-le sur votre système et vos navigateurs. 

Alors vous devrez l'accepter manuellement.

Un réseau `traefik` sera également créé.

## Comment l'utiliser

```shell script
make docker-upd
```

Avec cette commande, Traefik redémarrera automatiquement lorsque vous démarrerez votre ordinateur et plus besoin d'avoir un terminal ouvert, Traefik sera toujours en cours d'exécution.

```shell script
make docker-stop
```

Cette commande arrêtera traefik

Pour voir plus de commandes, utilisez la saisie semi-automatique après la commande make.
