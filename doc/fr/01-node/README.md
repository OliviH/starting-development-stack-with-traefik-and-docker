# NODE CONTAINER -  [(English version)](../../../01-node/README.md)

## How to use it

Créez un fichier `.env` et y coller le contenu du fichier `.env.example` ou créez un lien entre le fichier `.env.example` et le lien `.env` (voir l'exemple de la création du lien ci-dessous):

```bash
ln -s `pwd`/.env.example `pwd`/.env
```

Changer dans le fichier `.env` les termes `XXX` par les vôtres.

Par exemple en changeant les termes `XXX` par `my-app`, le lien du navigateur deviendra `http(s)://my-app.localhost`

## Commands

| commandes | Description |
|---|---|
| make build | build project |
| make build-start | build project and start in detach mode  |
| make clean | remove project and remove DATAS directory if exists |
| make help | see help |
| make start | start project in detach mode |
| make stop | stop project |

## Structure

> engine - dossier

C'est le dossier de backend de l'application. Ici il est juste configuré comme un petit serveur express en node.

> frontend/public - dossier

Ce dossier est le dossier servi par le serveur ci dessus
