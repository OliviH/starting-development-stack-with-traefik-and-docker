# NODE CONTAINER -  [(Version française)](../doc/fr/01-node/README.md)

## How to use it

Create `.env` file  and copy `.env.example` content into this one or create link between `.env.example` and `.env` file:

```bash
ln -s `pwd`/.env.example `pwd`/.env
```

Change `XXX` terms by yours

For example, by changing the `XXX` terms to `my-app`, the link in the browser will become `http(s)://my-app.localhost`

## Commands

| command | Description |
|---|---|
| make build | build project |
| make build-start | build project and start in detach mode  |
| make clean | remove project and remove DATAS directory if exists |
| make help | see help |
| make start | start project in detach mode |
| make stop | stop project |

## Structure

> engine folder

This is backend folder with node application

> frontend/public folder

This folder become frontend files served
