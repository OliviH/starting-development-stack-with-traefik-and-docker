import express from 'express'
const app = express()
app.use(express.static('/home/public'))
app.listen(3000, () => {
    console.log('Engine READY', new Date().toISOString())
})
